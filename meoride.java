class Multiply{
    Multiply(){
        System.out.println("This is to multiply two numbers ");
    }
    void mul(int a, int b){
        System.out.println("a*b= "+(a*b));
    }
    void mul(float a, float b){
        System.out.println("a*b= "+(a*b));
    }
}
class Main{
    public static void main(String[] args){
        Multiply o = new Multiply();
        o.mul(1,2);
        o.mul(1.0,2.0);
    }
}
